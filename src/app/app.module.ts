import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PluginComponent } from './components/plugin/plugin.component';
import { WatermarkpluginComponent } from './components/watermarkplugin/watermarkplugin.component';


@NgModule({
  declarations: [
    AppComponent,
    PluginComponent,
    WatermarkpluginComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
