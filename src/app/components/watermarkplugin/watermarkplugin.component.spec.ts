import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatermarkpluginComponent } from './watermarkplugin.component';

describe('WatermarkpluginComponent', () => {
  let component: WatermarkpluginComponent;
  let fixture: ComponentFixture<WatermarkpluginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatermarkpluginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatermarkpluginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
